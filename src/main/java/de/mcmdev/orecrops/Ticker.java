package de.mcmdev.orecrops;

import de.mcmdev.orecrops.api.Plant;
import de.mcmdev.orecrops.utils.Items;
import org.apache.commons.lang.math.RandomUtils;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.block.Skull;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.Objects;

/*
 * just pretend here is the class name
 *
 * 1.0.0 SNAPSHOT
 *
 * © 2018 MCMDEV
 */
public class Ticker implements Runnable {

    private OreCrops oreCrops;

    public Ticker(OreCrops oreCrops) {
        this.oreCrops = oreCrops;
    }

    @Override
    public void run() {
        for(Plant plant : oreCrops.getPlants().getPlants()) {
            plant.tick();
            if(plant.getTicks() > plant.getType().getTicks())   {
                Block block = plant.getLocation().getBlock();
                if(block.getType() != Material.PLAYER_HEAD) {
                    block.setType(Material.PLAYER_HEAD);
                    Skull skull = (Skull) block.getState();
                    skull.setPlayerProfile(Objects.requireNonNull(((SkullMeta) plant.getType().getItemStack().getItemMeta()).getPlayerProfile()));
                    skull.update(true);
                }
                block.getWorld().spawnParticle(Particle.VILLAGER_HAPPY, block.getLocation().add(0.5, 1, 0.5), 3, RandomUtils.nextDouble() - 0.5, RandomUtils.nextDouble() - 0.5, RandomUtils.nextDouble() - 0.5);
            }
        }
    }
}
