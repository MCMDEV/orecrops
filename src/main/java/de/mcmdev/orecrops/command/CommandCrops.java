package de.mcmdev.orecrops.command;

import de.mcmdev.orecrops.utils.Items;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/*
 * just pretend here is the class name
 *
 * 1.0.0 SNAPSHOT
 *
 * © 2018 MCMDEV
 */
public class CommandCrops implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player)    {
            Player player = (Player) sender;
            if(player.hasPermission("orecrops"))    {
                if(args.length == 2)    {
                    if(args[0].equalsIgnoreCase("give"))    {
                        if(args[1].equalsIgnoreCase("coal"))    {
                            player.getInventory().addItem(Items.coalPlant);
                            player.sendMessage("§aYou received a Coal Plant");
                        }
                        if(args[1].equalsIgnoreCase("iron"))    {
                            player.getInventory().addItem(Items.ironPlant);
                            player.sendMessage("§aYou received a Iron Plant");
                        }
                        if(args[1].equalsIgnoreCase("gold"))    {
                            player.getInventory().addItem(Items.goldPlant);
                            player.sendMessage("§aYou received a Gold Plant");
                        }
                        if(args[1].equalsIgnoreCase("diamond"))    {
                            player.getInventory().addItem(Items.diamondPlant);
                            player.sendMessage("§aYou received a Diamond Plant");
                        }
                    }
                }   else    {
                    player.sendMessage("§cUsage: /orecrops give <coal/iron/gold/ore>");
                }
            }
        }
        return false;
    }
}
