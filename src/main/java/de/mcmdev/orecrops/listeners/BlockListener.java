package de.mcmdev.orecrops.listeners;

import de.mcmdev.orecrops.OreCrops;
import de.mcmdev.orecrops.api.Plant;
import de.mcmdev.orecrops.utils.Items;
import org.apache.commons.lang.math.RandomUtils;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitTask;

import java.util.function.Consumer;

/*
 * just pretend here is the class name
 *
 * 1.0.0 SNAPSHOT
 *
 * © 2018 MCMDEV
 */
public class BlockListener implements Listener {

    private OreCrops oreCrops;

    public BlockListener(OreCrops oreCrops) {
        this.oreCrops = oreCrops;
    }

    @EventHandler
    public void onPlace(BlockPlaceEvent event)  {
        ItemStack itemStack = event.getItemInHand();

        for(Plant.Type type : Plant.Type.values())  {
            if(itemStack.isSimilar(type.getItemStack()))    {
                oreCrops.getPlants().addPlant(new Plant(event.getBlock().getLocation(), type, 0));
                //Scheduler is here because of Weird Spigot Bug
                oreCrops.getServer().getScheduler().runTaskLater(oreCrops, new Consumer<BukkitTask>() {
                    @Override
                    public void accept(BukkitTask bukkitTask) {
                        event.getBlock().setType(Material.OAK_SAPLING);
                    }
                }, 5);
            }
        }
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event)  {
        Block block = event.getBlock();

        Plant plant = oreCrops.getPlants().getPlant(block.getLocation());
        if(plant != null)  {
            if(plant.getTicks() > plant.getType().getTicks())   {
                event.setDropItems(false);
                event.setExpToDrop(plant.getType().getRarity());
                block.getWorld().dropItem(block.getLocation(), new ItemStack(plant.getType().getMaterial(), RandomUtils.nextInt(1) + 1));
                block.getWorld().dropItem(block.getLocation(), plant.getType().getItemStack());
            }   else    {
                event.setDropItems(false);
                event.setExpToDrop(0);
            }
        }
        oreCrops.getPlants().removePlant(plant);
    }

}
