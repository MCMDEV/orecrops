package de.mcmdev.orecrops;

import de.mcmdev.orecrops.command.CommandCrops;
import de.mcmdev.orecrops.listeners.BlockListener;
import de.mcmdev.orecrops.utils.Plants;
import org.bukkit.plugin.java.JavaPlugin;

public final class OreCrops extends JavaPlugin {

    private Plants plants;

    @Override
    public void onEnable() {
        plants = new Plants(this);
        getServer().getPluginManager().registerEvents(new BlockListener(this), this);
        getCommand("orecrops").setExecutor(new CommandCrops());

        getServer().getScheduler().runTaskTimer(this, new Ticker(this), 0, 20);
    }

    @Override
    public void onDisable() {
        plants.savePlants();
    }

    public Plants getPlants() {
        return plants;
    }
}
