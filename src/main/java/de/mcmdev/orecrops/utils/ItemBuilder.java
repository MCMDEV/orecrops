package de.mcmdev.orecrops.utils;
/*
 * just pretend here is the class name
 *
 * 1.0.0 SNAPSHOT
 *
 * © 2018 MCMDEV
 */

import com.destroystokyo.paper.profile.PlayerProfile;
import com.destroystokyo.paper.profile.ProfileProperty;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.*;

public class ItemBuilder {

    private ItemStack item;

    private Material material;
    private int amount = 1;
    private short subid = 0;
    private String displayName;
    private List<String> lore = new ArrayList<>();
    private Map<Enchantment, Integer> enchantments = new HashMap<>();
    private OfflinePlayer skullOwner;
    private UUID skullUuid;
    private String skullValue;
    private boolean unbreakable;
    private int customModelData;

    public ItemBuilder(Material material) {
        this.material = material;
    }

    public ItemBuilder(ItemStack item) {
        this.item = item;
    }

    public Material getMaterial() {
        return material;
    }

    public ItemBuilder setMaterial(Material material) {
        this.material = material;
        return this;
    }

    public int getAmount() {
        return amount;
    }

    public ItemBuilder setAmount(int amount) {
        this.amount = amount;
        return this;
    }

    public short getSubid() {
        return subid;
    }

    public ItemBuilder setSubid(short subid) {
        this.subid = subid;
        return this;
    }

    public int getCustomModelData() {
        return customModelData;
    }

    public ItemBuilder setCustomModelData(int customModelData) {
        this.customModelData = customModelData;
        return this;
    }

    public String getDisplayName() {
        return displayName;
    }

    public ItemBuilder setDisplayName(String displayName) {
        this.displayName = displayName;
        return this;
    }

    public List<String> getLore() {
        return lore;
    }

    public ItemBuilder setLore(List<String> lore) {
        this.lore = lore;
        return this;
    }

    public ItemBuilder addLore(String lore) {
        this.lore.add(lore);
        return this;
    }

    public ItemBuilder addEnchantment(Enchantment enchantment, int level) {
        this.enchantments.put(enchantment, level);
        return this;
    }

    public ItemBuilder setSkullOwner(OfflinePlayer skullOwner) {
        this.skullOwner = skullOwner;
        return this;
    }

    public boolean isUnbreakable() {
        return unbreakable;
    }

    public ItemBuilder setUnbreakable(boolean unbreakable) {
        this.unbreakable = unbreakable;
        return this;
    }

    public ItemBuilder setSkullData(UUID skullUuid, String skullValue) {
        this.skullUuid = skullUuid;
        this.skullValue = skullValue;
        return this;
    }

    public ItemStack generate() {
        item = new ItemStack(material, amount, subid);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(displayName);
        meta.setLore(lore);
        meta.setUnbreakable(isUnbreakable());
        if(!enchantments.isEmpty()) {
            enchantments.forEach((enchantment, integer) -> meta.addEnchant(enchantment, integer, true));
        }
        if(skullOwner != null)  {
            ((SkullMeta) meta).setOwningPlayer(skullOwner);
        }
        if(skullUuid != null && skullValue != null)    {
            PlayerProfile profile = Bukkit.createProfile(skullUuid);
            profile.setProperty(new ProfileProperty("textures", skullValue));
            profile.complete(false);
            ((SkullMeta) meta).setPlayerProfile(profile);
        }
        meta.setCustomModelData(customModelData);
        item.setItemMeta(meta);
        return item;
    }
}
