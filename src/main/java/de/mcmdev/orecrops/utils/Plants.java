package de.mcmdev.orecrops.utils;

import de.mcmdev.orecrops.OreCrops;
import de.mcmdev.orecrops.api.Plant;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.List;

/*
 * just pretend here is the class name
 *
 * 1.0.0 SNAPSHOT
 *
 * © 2018 MCMDEV
 */
public class Plants {

    private OreCrops oreCrops;
    private List<Plant> plants;

    public Plants(OreCrops oreCrops) {
        this.oreCrops = oreCrops;

        plants = loadPlants();
    }

    private List<Plant> loadPlants() {
        List<Plant> plants = new ArrayList<>();
        for(String key : oreCrops.getConfig().getKeys(false))    {
            Location location = (Location) oreCrops.getConfig().get(key + ".location");
            Plant.Type type = Plant.Type.valueOf(oreCrops.getConfig().getString(key + ".type"));
            int ticks = oreCrops.getConfig().getInt(key + ".ticks");
            plants.add(new Plant(location, type, ticks));
        }
        return plants;
    }

    public List<Plant> getPlants() {
        return plants;
    }

    public void addPlant(Plant plant)   {
        plants.add(plant);
    }

    public void removePlant(Plant plant)    {
        plants.remove(plant);
    }

    public Plant getPlant(Location location)    {
        return plants.stream().filter(plant -> plant.getLocation().equals(location)).findFirst().orElse(null);
    }

    public void savePlants()    {
        for(String key : oreCrops.getConfig().getKeys(false))    {
            oreCrops.getConfig().set(key, null);
        }
        for(Plant plant : plants)   {
            int id = plants.indexOf(plant);

            oreCrops.getConfig().set(id + ".location", plant.getLocation());
            oreCrops.getConfig().set(id + ".type", plant.getType().name());
            oreCrops.getConfig().set(id + ".ticks", plant.getTicks());
        }

        oreCrops.saveConfig();
    }
}
