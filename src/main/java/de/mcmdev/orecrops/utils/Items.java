package de.mcmdev.orecrops.utils;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

/*
 * just pretend here is the class name
 *
 * 1.0.0 SNAPSHOT
 *
 * © 2018 MCMDEV
 */
public class Items {

    public static ItemStack coalPlant = new ItemBuilder(Material.PLAYER_HEAD).setDisplayName("§8Coal Ore Plant").addLore("§7This plant will grow").addLore("§7coal ore every few minutes").setSkullData(UUID.fromString("562e0b0d-ae48-441b-958d-a529474d3d62"), "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYmU3OGQ4ZWI1ODgzYTExMDY2N2I1MWVlMjBhM2IyMWQ1MmQwODlhMGQ0YzkxYmUzY2I3ODZjMzE4MjhhMjA5NyJ9fX0=").generate();
    public static ItemStack ironPlant = new ItemBuilder(Material.PLAYER_HEAD).setDisplayName("§7Iron Ore Plant").addLore("§7This plant will grow").addLore("§7iron ore every few minutes").setSkullData(UUID.fromString("0390e683-5bf4-403f-b8d2-bd97d17d5827"), "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYjIyNjhjYzJmYjBhZjY3NWZmZjE3ZjE1YWY0YTZhYzI0MGJkYmY0MGE4Mzk2ZTAzNWJkMWE1YWViNGJjYzI3YyJ9fX0=").generate();
    public static ItemStack goldPlant = new ItemBuilder(Material.PLAYER_HEAD).setDisplayName("§5Gold Ore Plant").addLore("§7This plant will grow").addLore("§7gold ore every few minutes").setSkullData(UUID.fromString("42fe9a6a-fd89-47ec-8579-99bd569e0c66"), "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNzhlOTlkZGY5NTExZDM1YjU3OTA2N2Y2ZmI3OWEwYTJhNWI3N2ZjNGMyODk4YmJlZjU3MTJmOTg1M2NmYmQwNCJ9fX0=").generate();
    public static ItemStack diamondPlant = new ItemBuilder(Material.PLAYER_HEAD).setDisplayName("§bDiamond Ore Plant").addLore("§7This plant will grow").addLore("§7diamond ore every few minutes").setSkullData(UUID.fromString("2bdaab6d-34e9-4645-a415-69ef887ecc1b"), "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZmIxYzI2OGVmZWM4ZDdkODhhMWNiODhjMmJmYTA5N2ZhNTcwMzc5NDIyOTlmN2QyMDIxNTlmYzkzY2QzMDM2ZCJ9fX0=").generate();

}
