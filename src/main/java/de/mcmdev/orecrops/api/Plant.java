package de.mcmdev.orecrops.api;

import de.mcmdev.orecrops.utils.Items;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/*
 * just pretend here is the class name
 *
 * 1.0.0 SNAPSHOT
 *
 * © 2018 MCMDEV
 */
public class Plant {

    private Location location;
    private Type type;
    private long ticks;

    public Plant(Location location, Type type, long ticks) {
        this.location = location;
        this.type = type;
        this.ticks = ticks;
    }

    public Location getLocation() {
        return location;
    }

    public Type getType() {
        return type;
    }

    public void tick()  {
        ticks++;
    }

    public void addTicks(int tick) {
        ticks+=tick;
    }

    public long getTicks() {
        return ticks;
    }

    public enum Type  {

        COAL(60, Items.coalPlant, Material.COAL,1),
        IRON(100, Items.ironPlant, Material.IRON_ORE, 2),
        GOLD(120, Items.goldPlant, Material.GOLD_ORE, 3),
        DIAMOND(180, Items.diamondPlant, Material.DIAMOND, 4);

        private int ticks;
        private ItemStack itemStack;
        private Material material;
        private int rarity;

        Type(int ticks, ItemStack itemStack, Material material, int rarity) {
            this.ticks = ticks;
            this.itemStack = itemStack;
            this.material = material;
            this.rarity = rarity;
        }

        public int getTicks() {
            return ticks;
        }

        public ItemStack getItemStack() {
            return itemStack;
        }

        public Material getMaterial() {
            return material;
        }

        public int getRarity() {
            return rarity;
        }
    }
}

